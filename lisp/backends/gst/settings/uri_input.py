# -*- coding: utf-8 -*-
#
# This file is part of Linux Show Player
#
# Copyright 2012-2016 Francesco Ceruti <ceppofrancy@gmail.com>
#
# Linux Show Player is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Linux Show Player is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Linux Show Player.  If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtCore import QStandardPaths, Qt
from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QPushButton, QLineEdit, \
    QGridLayout, QCheckBox, QSpinBox, QLabel, QFileDialog, QVBoxLayout

from lisp.backends.gst.elements.uri_input import UriInput
from lisp.backends.gst.settings.settings_page import GstElementSettingsPage


class UriInputSettings(GstElementSettingsPage):
    NAME = 'URI Input'
    ELEMENT = UriInput

    def __init__(self, element_id, **kwargs):
        super().__init__(element_id)
        self.setLayout(QVBoxLayout())
        self.layout().setAlignment(Qt.AlignTop)

        self.fileGroup = QGroupBox('Source', self)
        self.fileGroup.setLayout(QHBoxLayout())
        self.layout().addWidget(self.fileGroup)

        self.buttonFindFile = QPushButton(self.fileGroup)
        self.buttonFindFile.setText('Find file')
        self.fileGroup.layout().addWidget(self.buttonFindFile)

        self.filePath = QLineEdit('file://', self.fileGroup)
        self.fileGroup.layout().addWidget(self.filePath)

        self.bufferingGroup = QGroupBox('Buffering', self)
        self.bufferingGroup.setLayout(QGridLayout())
        self.layout().addWidget(self.bufferingGroup)

        self.useBuffering = QCheckBox('Use buffering', self.bufferingGroup)
        self.bufferingGroup.layout().addWidget(self.useBuffering, 0, 0, 1, 2)

        self.download = QCheckBox(self.bufferingGroup)
        self.download.setText('Attempt download on network streams')
        self.bufferingGroup.layout().addWidget(self.download, 1, 0, 1, 2)

        self.bufferSize = QSpinBox(self.bufferingGroup)
        self.bufferSize.setRange(-1, 2147483647)
        self.bufferSize.setValue(-1)
        self.bufferingGroup.layout().addWidget(self.bufferSize, 2, 0)

        self.bufferSizeLabel = QLabel(self.bufferingGroup)
        self.bufferSizeLabel.setText('Buffer size (-1 default value)')
        self.bufferSizeLabel.setAlignment(Qt.AlignCenter)
        self.bufferingGroup.layout().addWidget(self.bufferSizeLabel, 2, 1)

        self.buttonFindFile.clicked.connect(self.select_file)

    def get_settings(self):
        conf = {self.id: {}}

        checkable = self.fileGroup.isCheckable()

        if not (checkable and not self.fileGroup.isChecked()):
            conf[self.id]['uri'] = self.filePath.text()
        if not (checkable and not self.bufferingGroup.isChecked()):
            conf[self.id]['use_buffering'] = self.useBuffering.isChecked()
            conf[self.id]['download'] = self.download.isChecked()
            conf[self.id]['buffer_size'] = self.bufferSize.value()

        return conf

    def load_settings(self, settings):
        if settings is not None and self.id in settings:
            self.filePath.setText(settings[self.id]['uri'])

    def enable_check(self, enable):
        self.fileGroup.setCheckable(enable)
        self.fileGroup.setChecked(False)

    def select_file(self):
        path = QStandardPaths.writableLocation(QStandardPaths.MusicLocation)
        file, ok = QFileDialog.getOpenFileName(self, 'Choose file', path,
                                               'All files (*)')

        if ok:
            self.filePath.setText('file://' + file)